import streamlit as st
import tools.home
import tools.search_on_dois
import tools.successful_submission_tool
import tools.reports_tool
import tools.api_downloader
import tools.dep_owner_checker
import tools.submission_downloader
import tools.home_page
import tools.api_url_csv_tool
import tools.doi_checker
import tools.invoice_checker
import requests
import awesome_streamlit as ast

st.image("https://assets.crossref.org/logo/crossref-logo-landscape-200.png")


tool_list = ['Home','Search on DOIs']#'Reports Tool','Owner Prefix Checker','Submission Downloader','Submission Tool','Rerun Successful Submissions','Bulk Upload Tool']





tools = {

"Home":tools.home,
"Home_MD": tools.home_page,
"Successful Submission Rerun":tools.successful_submission_tool,
"Search on DOIs":tools.search_on_dois,
"API Item Downloader":tools.api_downloader,
"Depositor Report Owner Checker":tools.dep_owner_checker,
"Submission Downloader" :tools.submission_downloader,
"Reports Tool":tools.reports_tool,
"API Query URL Download tool":tools.api_url_csv_tool,
"DOI checker" : tools.doi_checker,
"Quarterly Reports Downloader" : tools.invoice_checker

}



def init_login():
    credentials = st.sidebar.container()
    credentials.subheader("Enter email and password if required by tools")
    email = credentials.text_input("Email Address",key="email")
    password = credentials.text_input("Password", type="password", key="password")
    return email,password



def main():
    """Main function of the App"""
    st.sidebar.title("Tools choice")
    selection = st.sidebar.radio("Select", list(tools.keys()))

    tool = tools[selection]
    tool.main()


if __name__ == "__main__":
    init_login()
    main()
    