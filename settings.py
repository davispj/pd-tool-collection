CR_PRIMARY_COLORS = {
    "CR_PRIMARY_RED": "#ef3340",
    "CR_PRIMARY_GREEN": "#3eb1c8",
    "CR_PRIMARY_YELLOW": "#ffc72c",
    "CR_PRIMARY_LT_GREY": "#d8d2c4",
    "CR_PRIMARY_DK_GREY": "#4f5858",
}


API_URI = "https://api.crossref.org/"