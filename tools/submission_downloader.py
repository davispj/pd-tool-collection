
import streamlit as st
import requests
import datetime
import pandas as pd
import numpy as np
import urllib.parse
import urllib.request
from urllib.parse import unquote
from bs4 import BeautifulSoup as bs
import xml.etree.cElementTree as ET
import urllib.request as urllib2
from zipfile import ZipFile



def timestamp():
	return ('{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now()))



def submission_get(email,password,role,submission,file_id):
	st.session_state.filename = f"{file_id}_{submission}.xml"
	url = f'https://doi.crossref.org/servlet/submissionAdmin?sf=content&submissionID={submission}&usr={email}/{role}&pwd={password}'
	st.write(url)
	xml = requests.get(url).content
	soup = bs(xml,'xml')
	return create_xml_file(f'{soup}')

def create_xml_file(xml_data):
	with open (st.session_state.filename,"w") as f:
		f.write(xml_data)
		st.session_state.files.append(f)
	return f


def split_submissions(submissions):
	submissions = submissions.split()
	return submissions



def check_details(credentials):
	st.session_state
	for k,v in credentials.items():
		if not v:
			st.error(f"{k} has not been entered, please fill in and try again")
			return False
		else: get_role_list()
	return True

def get_role_list():
    st.session_state.roles_list = []
    url = "https://doi.crossref.org/servlet/login"
    mydata = {
        'usr': st.session_state.email,
        'pwd': st.session_state.password,

    }
    s = requests.Session()
    r = s.post(url,data=mydata).json()
    for item in r['roles']:
        st.session_state.roles_list.append(item)
    return st.session_state.roles_list


def main():
	
	st.session_state.files = []
	st.subheader("**Crossref Submission Download Tool**")
	form_body = st.form(key="sub_form", clear_on_submit=True)
	if check_details({'Email':st.session_state.email,'Password':st.session_state.password}) == True:
		form_body.selectbox("Username/role for download",key="role",options=st.session_state.roles_list)
		file_id = form_body.text_input("Enter an ID to append to the filenames")
		submissions = form_body.text_area("Enter the submission IDs here")
		st.session_state.submissions = split_submissions(submissions)
		download_files = form_body.form_submit_button('Download XML files')
		if download_files:
			zip_name = f"download_tool_{timestamp()}.zip"
			with ZipFile(zip_name, 'w') as zip_file:
				for submission in st.session_state.submissions:
					file = submission_get(st.session_state.email,st.session_state.password,st.session_state.role,submission,file_id)
					st.write(file)
					zip_file.write(file.name)
			with open(zip_name, 'rb') as f:
				st.download_button("Download Zip File",f,file_name=zip_name)

if __name__ =='__main__':
	main()
