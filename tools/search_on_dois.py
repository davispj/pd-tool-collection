
import streamlit as st
import requests
import datetime
import pandas as pd
import numpy as np
import urllib.parse
from urllib.parse import unquote

import xml.etree.cElementTree as ET



works_url = "works?"
nl = "\n"



#EMPTY LISTS
dois = []
failed_dois = []
success_dois = []
not_reg = []
filename_list = ['download']
column_list = []
data_list = []
custom_list = {
		
		'Title':'title',
		'Container title':'container-title',
		'Indexed date':'indexed',
		'Prefix':'prefix',
		'Issue':'issue',
		'Publisher':'publisher',
		'Type':'type',
		'Created date':'created',
		'Reference-count':'reference-count',
		'Page':'page',
		'Cited by count':'is-referenced-by-count',
		'Volume':'volume',
		'Authors':'author',
		'Member ID':'member',
		'Full text links':'link',
		'Published' : 'published',
		'Crossmark Domain' : 'content-domain',
		'Resource URL' : 'resource',
		'ISSN' : 'ISSN',
		'Subject' : 'subject',
		'Crossmark Policy DOI' : 'update-policy',
		'Timestamp' : 'indexed'

		}



def dict_convert_to_table(results):
	df = pd.DataFrame.from_dict(results,orient='index')
	return df


def headers():
  return {
      "User-Agent" : "crossref-api-downloader; mailto=support@crossref.org"
  }



def split_dois(dois):
	dois = dois.split()
	return dois

	
def convert_from_dict(data):
	for k,v, in data.items():
		if k == "date-parts":
			data = datetime.date(*v[0])
		elif k == "timestamp":
			data = v
		elif k == "domain":
			data = v

	return data


def custom_route(dois,custom_choice):
	doi_dict = {}
	for doi in dois:
		data_dict = {}
		try:
			api_call = "http://api.crossref.org/works/" + doi
			# st.write(api_call)
			results = requests.get(api_call).json()['message']
			for k,v in custom_list.items():
				for c in custom_choice:
					if c == k:
						try:
							data = results.get(v)
							# st.write(data)
							# st.write(c)
							if isinstance(data,list):
								if len(data) < 1:
									data = "No data"
									data_dict[k]=data
								else:
									if c == "ISSN":
										data = data
									else:
										data = data[0]
									data_dict[k]=data
							elif isinstance(data,dict):
								data = convert_from_dict(data)
								data_dict[k]=data
							else:
								data_dict[k]=data
						except (ValueError,KeyError,NameError):
							data = "No data"
							data_dict[k]=data
				doi_dict[doi] = data_dict
		except KeyError:
			pass 
	return doi_dict




def return_doi_list(dois):
	if len(dois) > 100:
		st.info(f"List not shown as over 100 DOIs  \n Total DOIs {len(dois)}")
		
		
	else:	
		st.sidebar.subheader(f"Here is the list of {len(dois)} DOIs to check on: ")
		dois = " ".join(dois)
		st.markdown('##')
		st.sidebar.info(dois)
		

def data_to_csv(results):
	data = pd.DataFrame(results)
	csv_data = data.to_csv(encoding='utf-8')
	return csv_data

def get_results(results,filename):
	with st.expander("Here are the results"):
		st.write(results)
		data = data_to_csv(results)
		download_data = st.download_button("📥  Download Dataframe",data,mime='text/csv',file_name=filename)
		return download_data



def main():
	st.subheader(f"Search on DOIs tool")
	timestamp = ('{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now()))
	dois_input = st.text_area("Enter DOIs here")
	if dois_input:
		st.session_state.dois = split_dois(dois_input)
		return_doi_list(st.session_state.dois)
		custom_choice = st.multiselect("What data do you want to return?",sorted(custom_list.keys()))
		
		filename_list.append("data_from_dois")
		filename_list.append(timestamp + ".csv")
		run_tool = st.button("Get the Data")
		filename = "_".join(filename_list)
		if run_tool:
			with st.spinner("Getting the data..."):
				if len(custom_choice) > 0:
					results_dict = custom_route(st.session_state.dois,custom_choice)
					results = dict_convert_to_table(results_dict)
					get_results(results,filename)
				else:
					st.write("No data requested for return, please select elements from list and try again")
		

if __name__ == "__main__":
	main()
		
				
	




			




		
# st.session_state

# https://test.crossref.org/servlet/submissionAdmin?sf=content&submissionID=1433241432