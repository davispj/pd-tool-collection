
"""
Created on Mon Mar  7 12:27:49 2022

@author: pauldavis
"""


import sys
from ast import parse
import streamlit as st
import requests
import datetime
import pandas as pd
import urllib.parse
from settings import (CR_PRIMARY_COLORS,API_URI)
from urllib.parse import unquote
import plotly.graph_objects as go
import plotly.express as px
from settings import API_URI
# st.write(st.__version__)


def works():
	return "works?"



def element_lists():
	route_names = ["Works Route", "Journal Route", "Prefix Route", "Member Route"]
	select_names = ['DOI' , 'ISBN' , 'ISSN' , 'URL' , 'abstract' , 'accepted' , 'alternative-id' , 'approved' , 'archive' , 'article-number' , 'assertion' , 'author' , 'chair' , 'clinical-trial-number' , 'container-title' , 'content-created' , 'content-domain' , 'created' , 'degree' , 'deposited' , 'editor' , 'event' , 'funder' , 'group-title' , 'indexed' , 'is-referenced-by-count' , 'issn-type' , 'issue' , 'issued' , 'license' , 'link' , 'member' , 'original-title' , 'page' , 'posted' , 'prefix' , 'published' , 'published-online' , 'published-print' , 'publisher' , 'publisher-location' , 'reference' , 'references-count' , 'relation' , 'score' , 'short-container-title' , 'short-title' , 'standards-body' , 'subject' , 'subtitle' , 'title' , 'translator' , 'type' , 'update-policy' , 'update-to' , 'updated-by' , 'volume']
	facet_names = ['placeholder', 'affiliation' , 'archive' , 'assertion' , 'assertion-group' , 'category-name' , 'container-title' , 'funder-doi' , 'funder-name' , 'issn' , 'journal-issue' , 'journal-volume' , 'license' , 'link-application' , 'orcid' , 'published' , 'publisher-name' , 'relation-type' , 'ror-id' , 'source' , 'type-name' , 'update-type']
	sort_names = sorted(['created' , 'deposited' , 'indexed' , 'is-referenced-by-count' , 'issued' , 'published' , 'published-online' , 'published-print' , 'references-count' , 'relevance' , 'score' , 'updated'])
	bool_filter_names = ['has-abstract' , 'has-affiliation' , 'has-archive' , 'has-assertion' , 'has-authenticated-orcid' , 'has-award' , 'has-clinical-trial-number' , 'has-content-domain' , 'has-description' , 'has-domain-restriction' , 'has-event' , 'has-full-text' , 'has-funder' , 'has-funder-doi' , 'has-license' , 'has-orcid' , 'has-references' , 'has-relation' , 'has-ror-id' , 'has-update' , 'has-update-policy' , 'is-update']
	date_filter_names = ['from-accepted-date' , 'from-approved-date' , 'from-awarded-date' , 'from-created-date' , 'from-deposit-date' , 'from-event-end-date' , 'from-event-start-date' , 'from-index-date' , 'from-issued-date' , 'from-online-pub-date' , 'from-posted-date' , 'from-print-pub-date' , 'from-pub-date' , 'from-update-date' , 'until-accepted-date' , 'until-approved-date' , 'until-awarded-date' , 'until-created-date' , 'until-deposit-date' , 'until-event-end-date' , 'until-event-start-date' , 'until-index-date' , 'until-issued-date' , 'until-online-pub-date' , 'until-posted-date' , 'until-print-pub-date' , 'until-pub-date' , 'until-update-date']
	option_filter_names = ['type']
	type_names = sorted(['book-section' , 'monograph' , 'report' , 'peer-review' , 'book-track' , 'journal-article' , 'book-part' , 'other' , 'book' , 'journal-volume' , 'book-set' , 'reference-entry' , 'proceedings-article' , 'journal' , 'component' , 'book-chapter' , 'proceedings-series' , 'report-series' , 'proceedings' , 'standard' , 'reference-book' , 'posted-content' , 'journal-issue' , 'dissertation' , 'grant' , 'dataset' , 'book-series' , 'edited-book' , 'standard-series'])
	return route_names,select_names,facet_names,sort_names,bool_filter_names,date_filter_names,option_filter_names,type_names

# def user_url_section():
# 	st.sidebar.info("Add your own API link below to retrieve results from\n **NB: Don't include the cursors or rows elements**")
# 	user_url_box = st.sidebar.text_input("Add the User URL here",key="user_url")
	
	
def init_form_sections():
	st.subheader("Facets & sorting")
	col1,col2 = st.columns([1,1])
	with col1.expander("Facets"):
		facets = [st.radio("Check the Facet you would like to use",element_lists()[2],key="facets",horizontal=True)]
	with col2.expander("Sort"):
		sorting = st.radio("What would you like to sort by",element_lists()[3],key="sorting",horizontal=True)
		order = st.select_slider("Select the order of the sort",options=["Ascending","Descending"],key='order')
	st.subheader("Selects")
	selects = st.multiselect("Select the elements to retrieve", element_lists()[1],key="selects")
	st.markdown("***")
	
	return selects,facets,sorting,order


# LISTS TO FILL
def global_lists():
	global filter_list,filename_list,url_list,facet_list,sort_list
	filter_list = []
	filename_list = ["download"]
	url_list = []
	facet_list = []
	sort_list = []


# @st.cache()
# def define_types():
# 	types = requests.get("https://api.crossref.org/types").json()['message']['items']
# 	type_names = ["" + label['id']for label in types]
# 	return type_names


def check_doi(url,route,name):
	r = requests.head(url)
	if r.status_code == 200:
		st.success(f"The {name} {route} is valid")
		check = True
	elif r.status_code == [504,500]:
		st.error(f"{r.status_code} Error returned...the API could be down")
		check = False
	else:
		st.warning(f"The {name} {route} is not valid, please check and try again")
		check = False
	return check


def run_total_results(url):
	data_url =f"{url}rows=0&facet=type-name:"
	r = requests.get(data_url).json()['message']
	total_results = r['total-results']
	content_types = r['facets']['type-name']['values']
	st.info(f"The member has **{total_results}** DOIs registered.  \n Comprising of the content types:  \n{content_types}")
	df = pd.DataFrame(content_types.items())
	return df

# def init_total_results(url):
# 	total_results = st.button("Member's Total DOIs and content type breakdown")
# 	if total_results:
# 		df = run_total_results(url)
# 		st.write("Content type")
# 		st.write(df)
# 		st.dataframe(df)
	

def check_existence(url,route,name):
	r = requests.head(url)
	if r.status_code == 200:
		st.success(f"The {name} {route} is valid")
		member_data = get_publisher_name(url,route,name)
		# init_total_results(url)
	elif r.status_code == [504,500]:
		st.error(f"{r.status_code} Error returned...the API could be down")
		sys.exit
	else:
		st.warning(f"The {name} {route} is not valid, please check and try again")
	return member_data


def headers():
  return {
      "User-Agent" : "crossref-api-downloader; mailto=support@crossref.org"
  }


def data_request(url):
	item_list = []
	cursor = "*"
	rows = "1000"
	while True:    
		if url[-1] == "?":
			full_query= str(f"{url}cursor={cursor}&rows={rows}")
		else:
			full_query= str(f"{url}&cursor={cursor}&rows={rows}")
		res = requests.get(full_query,headers=headers())
		if res.status_code == 200:
			success = "Got JSON...loading results"
		elif str(res.status_code).startswith('5'):
			st.error(f"{res.status_code} Error returned...the API could be down")
			
		else:
			st.warning(res.status_code)
			st.warning("Valid JSON has not been retrieved")
			
		facet_return = facet_request(res.json())
		try:
			body = res.json()['message']
			total_results = body['total-results']
			items = body['items']
			if not items:
				break
			cursorRaw = str(body['next-cursor'])
			cursor = urllib.parse.quote(cursorRaw)
			for i in items:
				item_list.append(i)
		except TypeError:
			st.warning(f"The API query is not valid or no results available, please clear form and try again")
			break
	return item_list,total_results,facet_return,success

def facet_request(json):
	return facet_list.append(json['message']['facets'])
	 
	
def init_bool_filters():
	bool_filters = st.multiselect("Filters",element_lists()[4])
	if bool_filters:
		bool_list = bool_filter_url(bool_filters)
		return bool_list


def init_date_filters():
	date_filters = st.multiselect("Date Filters",element_lists()[5])
	if date_filters:
		date_list = date_filter_url(date_filters)
		return date_list


def init_choice_filters():
	choice_filters = st.multiselect("Choice Filters",element_lists()[6])
	if choice_filters:
		choice_list = choice_filter_url(choice_filters)
		return choice_list


def init_query_box():
	query_box = st.text_input("Query Text")
	if query_box:
		query = query_url(query_box)
		return query



def query_url(query):
	url_list.append(f"query={query}")
	

def choice_filter_url(names):
	for name in names:
		if name == "type":
			choice_option = st.radio("Which type do you want to filter with",element_lists[7])
			filter_list.append(f"{name}:{choice_option}")
		else:
			pass
	return filter_list


def bool_filter_url(names):
	for name in names:
		boolean_option = st.radio("",["","true","false"],key=name,format_func=lambda x: name + " " + str(x))
		if boolean_option == "false":
			filter_list.append(f"{name}:f")
		else:
			filter_list.append(f"{name}:t")
	return filter_list

def date_filter_url(names):
	for name in names:
		date_entry = st.date_input("Enter " + name,key=name)
		filter_list.append(f"{name}:{date_entry}")	
	return filter_list


def route_builder(route_name,route_value):
	url = (f"{API_URI}{route_name}/{route_value}/{works()}")
	return url

# def check_details(route,name,description):	
# 		url = route_builder(route,name)
# 		filename_list.append(f"{description}{name}")
# 		if "url" not in st.session_state:
# 			st.session_state.url = url
# 			check = check_existence(url,name,description)
# 			if check == True:
# 				return url


def route_option():
	if st.session_state.route == "Journal Route":
		st.subheader("Journal route selected, add an ISSN below")
		issn = st.text_input("ISSN",help="Enter a valid ISSN here")
		if issn:
			url = route_builder("journals",issn)
			filename_list.append("journal" + issn)
			if "url" not in st.session_state:
				st.session_state.url = url
				check = check_existence(url,issn,"issn")
				if check == True:
					return url
			

	if st.session_state.route == "Prefix Route":
		st.subheader("Prefix route selected, add a prefix below")
		prefix = st.text_input("Prefix",help="Enter a Crossref prefix here")
		if prefix:
			url = route_builder("prefixes",prefix)
			filename_list.append("prefix_" + prefix)
			if "url" not in st.session_state:
				st.session_state.url = url
				check = check_existence(url,prefix,"prefix")
				if check == True:
					return url
			

	if st.session_state.route == "Member Route":
		st.subheader("Member route selected, add a memberID below")
		mem_id = st.text_input("Member ID",help="Enter a Crossref memberID here")	
		if mem_id:
			url = route_builder("members",mem_id)
			if "url" not in st.session_state:
				st.session_state.url = url
				check = check_existence(url,mem_id,"member ID")
				if check == True:
					filename_list.append("member_" + mem_id)
					return url
			
	
	if st.session_state.route == "Works Route":
		st.subheader("Works route selected")
		url = "https://api.crossref.org/works?"
		if "url" not in st.session_state:
				st.session_state.url = url
		return url



def get_publisher_name(url,route,name):
	new_url = url[:-6]
	r = requests.get(new_url).json()['message']
	if name == 'prefix':
		label = "Publisher"
		info = r['name']
	elif name == 'member ID':
		label = 'Publisher'
		info = r['primary-name']
	else:
		label = 'Journal title'
		info = 	r['title']
	st.info(label + ' : ' + info)
	return info


def select_url(names):	
	select_list = []
	for name in names:
		select_list.append(name)	
	return select_list


def facet_url(names):	
	facet_list = []
	for name in names:
		facet_list.append(name + ":%2A")	
	return facet_list


def convert_output(output):
	data = pd.DataFrame(output)
	csv_data = data.to_csv(index=False,encoding='utf_8')
	return csv_data


def sim_check_check(url):
	rows = "rows=0"
	total_results_url = url + rows
	r = requests.get(total_results_url,headers=headers()).json()['message']
	total_results = r['total-results']
	sim_check_url = url + "filter=has-full-text:t,full-text.application:similarity-checking" 
	s = requests.get(sim_check_url + "&" + rows,headers=headers()).json()['message']
	sim_check_results = s['total-results']
	st.info(f"Member has **{total_results}** total DOIs and **{sim_check_results}** that do not have Similarity Check URLs")
	sim_check_total = int(total_results / sim_check_results)
	fig = go.Figure(
        go.Indicator(
            mode="number+gauge",
            value=sim_check_total,
            number={
                "suffix": "%",
                "font": {"color": CR_PRIMARY_COLORS["CR_PRIMARY_YELLOW"], "size": 24},
            },
            gauge={
                "shape": "bullet",
                "axis": {"range": [None, 100]},
                "bar": {
                    "color": CR_PRIMARY_COLORS["CR_PRIMARY_GREEN"],
                    "line": {"width": 0},
                    "thickness": 1,
                },
                "bgcolor": "#ffffff",
            },
            title={
                "text": "Similarity Check Percentage",
                "font": {"color": CR_PRIMARY_COLORS["CR_PRIMARY_YELLOW"], "size": 18},
            },
        ),
        layout={
            "autosize": False,
            "width": 600,
            "height": 30,
            "margin": dict(l=300, r=20, b=5, t=5, pad=4),
            # "paper_bgcolor": CR_PRIMARY_COLORS["CR_PRIMARY_DK_GREY"],
            "xaxis_title": "Wangfo",
        },
    )

	st.plotly_chart(fig)
	return sim_check_total,sim_check_url,sim_check_results



def sim_check():
	with st.spinner("Running Similarity Check Query"):
		s_c_data = sim_check_check(st.session_state.url)
		if s_c_data[2]> 100000:
			st.info(f"This member has over 100,000 DOIs without Similarity Check URLs.  \n Do you want to proceed with the download report?")
			user_answer = st.radio("",["Yes","No"])
			if user_answer.lower() == "no":
				st.info("Cancelling request")
				pass
		else:
			sim_check_query = s_c_data[1]	
			s_c_output = data_request(sim_check_query + "&select=DOI")[0]
			s_c_data = convert_output(s_c_output)
			filename = "similarity_" + "_".join(filename_list) + "_" + timestamp() + ".csv"
			st.download_button("Download DOI list without Similarity Check",s_c_data, mime='text/csv',file_name=filename)	



def timestamp():
	return ('{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now()))




def main():
	st.subheader("**Crossref API downloader Tool**")
	st.markdown("***")
	global_lists()	
	st.session_state.route = st.radio("Which API route to use", element_lists()[0],horizontal=True)

	if st.session_state.route:
		url = route_option()
		init_date_filters()
		init_bool_filters()
		init_choice_filters()
	
		init_form_sections()	

		sim_check_button = st.button("Check Similarity Check Score")
			
		if filter_list:
			joined_list = ','.join(filter_list)
			filter_url = "filter=" + joined_list
			url_list.append(filter_url)
			
		if st.session_state.selects:
			s_list = select_url(st.session_state.selects)
			select_joined = ",".join(s_list)
			selects_url = "select=" + select_joined
			url_list.append(selects_url)
		
		if st.session_state.facets:	
			if (st.session_state.facets)[0] == "placeholder":
				pass
			else:
				f_list = facet_url(st.session_state.facets)
				facet_joined = ",".join(f_list)
				facets_url = "facet=" + facet_joined
				url_list.append(facets_url)
		
		if st.session_state.sorting:
			if (st.session_state.sorting)[0] == "":
				pass
			else:			
				sorting_url = "sort=" + st.session_state.sorting
				if st.session_state.order == "Ascending":
					order_url = "order=asc"
				else:
					order_url = "order=desc"
				url_list.append(sorting_url)
				url_list.append(order_url)

		if sim_check_button:
			sim_check()

	st.markdown("Enter a query string to search against in the API. Each word will be searched against individually")
	init_query_box()

	col1,col2,col3 = st.columns([1,1,1])
	run_query = col2.button("Run API Query")
	if run_query:
		api_query = st.session_state.url + "&".join(url_list)	
		with st.spinner("Retrieving your JSON..."):
			st.write(f'Here is link to the API query you are running:')
			st.info(unquote(api_query))
			col2.success(data_request(api_query)[3])
			st.info(f"**The total results from that query are {data_request(api_query)[1]}**")
			output = data_request(api_query)[0]
		if facet_list[0]:
			with st.expander(f"Facet section of your results"):
				st.json(facet_list[0])	
		st.subheader(f"Your results are here")
		with st.expander(f"Results"):
			st.json(output)
		filename = "_".join(filename_list) + "_" + timestamp() + ".csv"
		data = convert_output(output)
		col2.download_button("Download Ouput",data, mime='text/csv',file_name=filename)

if __name__ == '__main__':
	st.session_state
	main()
	
