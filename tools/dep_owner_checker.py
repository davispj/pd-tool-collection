import streamlit as st
import requests
import pandas as pd
from collections import Counter
from bs4 import BeautifulSoup as bs



def get_data(id):
	types = "J","B","S"
	for type in types:
		url = f"https://data.crossref.org/depositorreport?pubid={type}{id}"
		page = requests.get(url).text
		df = pd.DataFrame([x.split() for x in page.split("\n")])
		df.columns = df.iloc[0]
		df = df.iloc[:-1,:]
		owner_prefixes = list(df["OWNER"])
		# st.write(owner_prefixes)
		if len(owner_prefixes) > 2:
			del owner_prefixes[0:1]
			owner_prefixes.remove(None)
			counter = Counter(owner_prefixes)
			return dict(counter)
	else:
		st.warning("That doesn't look like a valid cite ID, please try again")


def c_types_dict():
	content_types = {
	"101" : "Book",
	"102" : "Conference Proceeding",
	"103" : "Dissertation", 
	"104" : "Report",
	"105" : "Standard",
	"106" : "Database",
	"107" : "Conference Series",
	"108" : "Book Series",
	"109" : "Report Series",
	"110" : "Standard Series", 
	"111" : "Book Set",
	"112" : "Posted Content",
	"113" : "Grants", 
	}
	return content_types




def get_title():
	for c_type in c_types_dict().keys():
		url = f"https://doi.crossref.org/servlet/metadataAdmin?func=modify&btype={c_type}&citeId={st.session_state.cite_id}&usr={st.session_state.email}/root&pwd={st.session_state.password}"
		html_text = requests.get(url).text
		soup = bs(html_text, "html.parser")
		title = soup.find("input",attrs={"name":"title"})["value"]
		content_type = c_types_dict()[c_type]
		return title,content_type


def any_info_missing():
	return any(st.session_state[key] == "" for key in ["email","password"])

	

	
def check_inputs():
    if any_info_missing():
       return st.warning(
            "You have not provided all the needed information. Please enter your email address and password"
        )


def main():
	st.subheader("**Crossref Owner Prefix Checker**")
	with st.form("Form",clear_on_submit=True):
		cite_id = st.text_input("Enter the journal cite ID",key="cite_id")
		run_form = st.form_submit_button("Run Form")
		if run_form:
			if not cite_id:
					st.warning("Please enter a cite ID")
			else:
				check = check_inputs()
				if not check:	
					with st.spinner("Getting the list of DOIs"):
						data = get_data(cite_id)
						if data:
							title,content_type = get_title()[0],get_title()[1]
							st.success(f"Depositor report found and prefixes below")
							st.markdown("##")
							st.info(f"Here are the owner prefixes for {content_type} :  **{title}**")
							df = pd.DataFrame(list(data.items()),columns=["Owner Prefix","No of DOIs"])
							df.index +=1
							st.dataframe(df)
		

if __name__ == "__main__":
	main()








# URL = "https://data.crossref.org/depositorreport?pubid=J305869"


