
"""Home page shown when the user enters the application"""


import streamlit as st


def load_file(fn):
    with open(fn) as f:
        return f.read()

# pylint: disable=line-too-long
def main():
    """Used to write the page in the app.py file"""
    with st.spinner("Loading Home ..."):
       st.markdown(load_file("tools/home_page.md"))
          