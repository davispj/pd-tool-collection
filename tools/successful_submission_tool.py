
import streamlit as st
import requests
import datetime
import pandas as pd
import numpy as np
import urllib.parse
import urllib.request
from urllib.parse import unquote
import xml.etree.cElementTree as ET
from bs4 import BeautifulSoup as bs
import urllib.request as urllib2



def find_username_filename(email,password,submission):
	data = {}
	url = f'https://doi.crossref.org/servlet/submissionAdmin?sf=detail&submissionID={submission}&usr={email}/root&pwd={password}'
	xml = urllib2.urlopen(url).read()
	soup = bs(xml,'lxml')
	for table in soup.find_all('td',{"class" : "tabContent2"}):
		sections = table.find_all('tr')
		for s in sections[0:9]:
			k,v = ((s.get_text()).split(':',1))
			data[k] = v
	username = data['Depositor']
	filename = data['Name']
	return username,filename

def form_data(email,password,role):
	mydata = {
	'operation':'doMDUpload',
	'login_id': f'{email}/{role}',
	'login_passwd': password,

	}
	return mydata


def update_timestamp(xml):
	soup = bs(xml,'xml')
	new_timestamp = int(soup.find('timestamp').text) +1
	timestamp = soup.timestamp
	timestamp.string = str(new_timestamp)
	download_xml(f'{soup}')
	return f'{soup}'
	

def timestamp_now():
	return ('{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now()))


def submission_get(email,password,submission):
	url = f'https://doi.crossref.org/servlet/submissionAdmin?sf=content&submissionID={submission}&usr={email}/root&pwd={password}'
	xml = requests.get(url).content
	updated_xml = update_timestamp(xml)
	return create_xml_file(updated_xml)

def check_details(credentials):
	st.session_state
	for k,v in credentials.items():
		if not v:
			st.warning(f"{k} has not been entered, please fill in and try again")
		check = False
	else: check = True
	return check

def get_role_list():
	st.session_state.roles_list = []
	url = "https://doi.crossref.org/servlet/login"
	mydata = {
		'usr': st.session_state.email,
		'pwd': st.session_state.password,

	}
	s = requests.Session()
	r = s.post(url,data=mydata).json()
	for item in r['roles']:
		st.session_state.roles_list.append(item)
	# st.write(st.session_state.roles_list)
	return st.session_state.roles_list	

def deposit_file(mydata,file):
	deposit_file_button = st.button("Deposit New File",key="deposit_file_button")
	if deposit_file_button:
		upload_process(mydata,file)

def download_xml(xml):
	return st.download_button("Download XML",xml,file_name=st.session_state.filename)


def upload_process(mydata,xml_file):
	success = []
	failures = []
	sub_log = []
	sub_log_results = st.empty()
	with open (st.session_state.filename, "r") as f:
		file = {'fname':f}
		r = requests.post("https://doi.crossref.org/servlet/deposit", files=file, data=mydata)
		if r.status_code == 200:
			success.append(xml_file.name)
			sub_log.append(f"##{xml_file.name}## was successfully sent to the queue")
		else:
			failures.append(xml_file.name)
			st.warning(f"The file ##{xml_file.name}## has failed to reach the queue, please try again later")
		sub_log_results.success(f"Successfully sent to queue = {xml_file.name}		:white_check_mark: ")###{nl}Failed to send = {len(failures)}/{len(upload_files)}")


def create_xml_file(xml_data):
	with open (st.session_state.filename,"w") as f:
		f.write(xml_data)
	return f

def main():
	st.subheader("**Crossref Successful Submission Tool**")
	tool_body = st.container()
	submission = tool_body.text_input("Enter the submission ID here",key='submission_id').strip()
	if submission:
		if check_details({'Email':st.session_state.email,'Password':st.session_state.password}):
			get_role_list()

			username,st.session_state.filename = find_username_filename(st.session_state.email,st.session_state.password,submission)[0],find_username_filename(st.session_state.email,st.session_state.password,submission)[1]
			
			updated_xml = submission_get(st.session_state.email,st.session_state.password,submission)
			st.success("XML File and timestamp updated and ready to be downloaded or deposited")
			if username in st.session_state.roles_list:
				st.info(f'You have permissions for username **"{username}"** on your account.  \n Click "Deposit New File" button below to deposit new file.')
			else: st.info(f'Username **"{username}"** is not assigned against your auth account, please add it before clicking "Deposit New File" button below.')
			my_data = form_data(st.session_state.email,st.session_state.password,username)
			deposit_file(my_data,updated_xml)

if __name__ == '__main__':
	main()
st.session_state