import sys
from ast import parse
import streamlit as st
import requests
import datetime
import pandas as pd
import urllib.parse
from settings import (CR_PRIMARY_COLORS,API_URI)
from urllib.parse import unquote
import requests
from settings import API_URI





query_dict = {
    
    "Number of unique records with funding data" : "works?filter=has-funder:true",
    "Number of unique records with Funder IDs" : "works?filter=has-funder-doi:true",
    "Records with license and full text links" : "works?filter=has-license:true,has-full-text:true",
   # "Members registering license and full text links" : "works?filter=has-license:true,has-full-text:true&facet=publisher-name:*&rows=0",
    "Records with one or more authors with ORCID iDs" : "works?filter=has-orcid:t",
    #"Members registering ORCID iDs" : "works?filter=has-orcid:true&facet=publisher-name:*&rows=0",
    "Number of preprint-to-article links" : "works?filter=relation.type:is-preprint-of",
    "Number of works with references" : "works?filter=has-references:true",
    #"Members registering references" : "works?filter=has-references:true&facet=publisher-name:*&rows=0",
    "Number of works with open references" : "works?filter=has-references:true,reference-visibility:open",
    #"Members with open references" : "works?filter=has-references:true,reference-visibility:open&facet=publisher-name:*&rows=0",
    "Crossmark status updates" : "works?filter=is-update:true",
    "Crossmark content items" : "works?filter=has-update-policy:true",
    #"Members registering clinical trial numbers" : "works?filter=has-clinical-trial-number:true&facet=publisher-name:*&rows=0",
    "Records with clinical trial numbers" : "works?filter=has-clinical-trial-number:true"


}

def type_data():
	type_list_dict = {}
	for i,l in zip([i['label']for i in requests.get(f"{API_URI}types").json()['message']['items']],[i['id']for i in requests.get(f"{API_URI}types").json()['message']['items']]):
		type_list_dict[i] = l
	return type_list_dict 

def clean_up_dict(content_type_dict):
	del content_type_dict['Other']
	del content_type_dict['Part']
	del content_type_dict['Track']
	del content_type_dict['Entry']
	del content_type_dict['Book Series']
	del content_type_dict['Book Set']
	del content_type_dict['Proceedings Series']
	del content_type_dict['Section']
	return content_type_dict

def content_type_counts():
    content_type_url = f"{API_URI}works?facet=type-name:*&rows=0"
    r = requests.get(content_type_url).json()['message']
    total_results = r['total-results']
    no_of_content_types = r['facets']['type-name']['value-count']
    content_type_dict = r['facets']['type-name']['values']
    clean_up_dict(content_type_dict)
    return total_results,no_of_content_types,content_type_dict


def get_query_counts(query_url):
    r = requests.get(f"{API_URI}{query_url}").json()['message']
    total_results = r['total-results']
    return total_results
    

def query_counts_created_date(query_url,until_created_date,created_date_url,rows):    
    r = requests.get(f"{API_URI}{query_url}{created_date_url}{until_created_date}{rows}").json()['message']
    total_results = r['total-results']
    return total_results
    
def total_query_counts():
	results_dict = {}
	for k,v in query_dict.items():
		results = get_query_counts(v)
		results_dict[k] = results
	return results_dict

def counts_by_date(user_input_date,created_date_url,rows):
	created_results_dict = {}
	for k,v in query_dict.items():
		results = query_counts_created_date(v,user_input_date,created_date_url,rows)
		created_results_dict[k] = results
	return created_results_dict

def member_counts_per_type(content_type):
	member_counts_per_type_dict = {}
	for i,l in content_type.items():
		r = requests.get(f"{API_URI}types/{l}/works?facet=publisher-name:*&rows=0").json()
		data = r['message']['facets']['publisher-name']['value-count']
		member_counts_per_type_dict[f"No of members depositing {i}"] = data
	return member_counts_per_type_dict	

def convert_output(output):
	data = pd.DataFrame(output)
	csv_data = data.to_csv(index=False)
	return csv_data

def dict_convert_to_table(results):
	df = pd.DataFrame.from_dict(results,orient='index')
	return df

def headers():
  return {
      "User-Agent" : "crossref-api-downloader; mailto=support@crossref.org"
  }

def data_to_csv(results):
	data = pd.DataFrame(results)
	csv_data = data.to_csv(encoding='utf-8')
	return csv_data

def get_results(results):
	st.dataframe(results)
	data = data_to_csv(results)
	download_data = st.download_button("📥  Download Dataframe",data,mime='text/csv',file_name=st.session_state.filename)
	return download_data

def main():
	created_date_url = ",until-created-date:"
	rows = "&rows=0"
	st.subheader("**Crossref Reports Tool**")
	filename_list = ["download"]
	timestamp = ('{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now()))
	type_list = [i['id']for i in requests.get(f"{API_URI}types").json()['message']['items']]
	user_input_date = st.date_input("What date would you like the data up until: ")
	get_data = st.button("Get Data")

	if get_data:
		with st.spinner("Getting the data"):
			st.session_state.filename = "_".join(filename_list) + "_" + timestamp + ".csv"
			total_results,no_of_content_types,content_type_dict = content_type_counts()
			results_dict = total_query_counts()
			
			full_results_dict = content_type_dict | counts_by_date(user_input_date,created_date_url,rows) | member_counts_per_type(type_data()) | results_dict
			with st.expander(f"Results"):
				results = dict_convert_to_table(full_results_dict)
				get_results(results)

if __name__ == "__main__":
	main()




