import streamlit as st
import requests
import datetime
import pandas as pd
import urllib.parse
from urllib.parse import unquote
import xml.etree.cElementTree as ET
from bs4 import BeautifulSoup as bs
from collections import Counter

# from streamlit.script_runner import RerunException
# from streamlit_autorefresh import st_autorefresh


st.image("https://assets.crossref.org/logo/crossref-logo-landscape-200.png")
login_details = st.container()
col1,col2,col3 = login_details.columns([3,3,2])



works_url = "works?"
nl = "\n"
ret = "\r"
timestamp = ('{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now()))
st.markdown(""" <style>div[role="radiogroup"] >  :first-child{display: none !important;}</style>""",unsafe_allow_html=True)
st.write('<style>div.row-widget.stRadio > div{flex-direction:row;}</style>', unsafe_allow_html=True)
custom_container = st.container()
# st.markdown('##')
# st.markdown('##')
filename_list = []



upload_types_dict = {
"" : "",
"Metadata Upload" : "doMDUpload",
"URL updates/Title transfers" : "doTransferDOIsUpload" 

}





form_body = st.form("Deposit Form", clear_on_submit=True)
sub_checker = st.form("Check submissions")

form_body.subheader("**Crossref bulk upload tool**")


def init_login():
	with st.container():
		email = col1.text_input("Email Address",key="email")
		password = col2.text_input("Password", type="password", key="password")
		role = col3.text_input("Username/role for upload",key="role")
		upload_type = form_body.radio("Upload Type",upload_types_dict.keys(),key="upload_type",index=1)
		form_body.markdown('##')
	return email,password,role,upload_types_dict[upload_type]



def form_data(operation,email,password,role):
	mydata = {
	'operation':operation,
	'login_id': f'{email}/{role}',
	'login_passwd': password,

	}
	return mydata



def upload_process(mydata,environment):
	success = []
	failures = []
	sub_log = []
	sub_log_results = form_body.empty()
	for f in upload_files:
		
		myfile = {'fname':f}
		r = requests.post(f"{environment}deposit", files=myfile, data=mydata)
		if r.status_code == 200:
			success.append(f.name)
			sub_log.append(f"##{f.name}## was successfully sent to the queue")
		else:
			failures.append(f.name)
			form_body.warning(f"The file ##{f.name}## has failed to reach the queue, please try again later")
		sub_log_results.success(f"Successfully sent to queue = {len(success)}/{len(upload_files)} 		:white_check_mark: ")###{nl}Failed to send = {len(failures)}/{len(upload_files)}")
	# with form_body.expander("More "):
	# 	st.dataframe(sub_log)


def environ_url(env):
	if env == "Test":
		url = "https://test.crossref.org/servlet/"
	elif env == "Production":
		url = "https://doi.crossref.org/servlet/"
	return url


	#functions for the submission checker
def headers():
  return {
      "User-Agent" : "crossref-api-downloader; mailto=support@crossref.org"
  }


def submission_return(filename_list,email,password,role,base_url):
	for filename in filename_list:
		url = f'{base_url}submissionDownload?usr={email}/{role}&pwd={password}&file_name={filename}&type=result'
		# st.write(url)
		r = requests.get(url)
		sub_log,submission_id,status_log_raw = get_submission_log(r.text)[0],get_submission_log(r.text)[1],Counter(get_submission_log(r.text)[2])
		status_log = '\n'.join("{}: {}".format(k, v) for k, v in dict(status_log_raw).items())
		with st.expander(f"Submission ID:  {submission_id}  Filename:  {filename} | Breakdown:  {status_log}"):
			st.dataframe(dict_convert_to_table(sub_log))


	# return sub_logs
		


def get_submission_log(log):
	status_log = []
	record_log_dict = {}
	soup = bs(log, 'html.parser')
	submission_id = soup.find('submission_id').text
	sub_status = soup.find('doi_batch_diagnostic')['status']
	with st.spinner("Retrieving the submission log..."):
		if sub_status == 'unknown_submission':
			status = "Deposit still running, check again soon"		
		else:
			for record in soup.find_all('record_diagnostic'):
				status = record['status']
				status_log.append(status)
				doi = record.find('doi')
				message = record.find('msg')
				record_log_dict[doi.text] = [status,message.text]
	return record_log_dict,submission_id,status_log ##submission_log_dict

def dict_convert_to_table(results):
	df = pd.DataFrame.from_dict(results,orient='index',columns=['Status','Message'])
	pd.set_option('display.max_colwidth', 40)
	return df

def check_details(credentials):
	for k,v in credentials.items():
		if not v:
			st.warning(f"{k} has not been entered, please fill in and try again")


email,password,role,upload_type = init_login()
system_choice = form_body.radio("Choose an environment to upload to (default is 'Test')", ["","Production","Test"],index=2,key='system_choice')
form_body.markdown('##')
upload_files = form_body.file_uploader("Upload the files to deposit",type=['xml','txt'], accept_multiple_files=True,key="files")
if upload_files:
	for u in upload_files:
		filename_list.append(u.name)
# st.write(filename_list)
deposit_files = form_body.form_submit_button("Deposit Files")
if deposit_files:
	check_details({'Email':email,'Password':password,'Role':role,'Test or production':system_choice,'Upload type':upload_type,'Files':upload_files})
	upload_process(form_data(upload_type,email,password,role),environ_url(system_choice))




sub_checker.subheader("Check the submission logs")
check_submissions = sub_checker.form_submit_button("Check the submissions")
if check_submissions:
	(submission_return(filename_list,email,password,role,environ_url(system_choice)))
	# sub_checker.dataframe(results)
# st.session_state