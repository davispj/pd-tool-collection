import streamlit as st
import requests
import datetime
import pandas as pd
import numpy as np
import urllib.parse
from urllib.parse import unquote
import plotly.graph_objects as go
import plotly.express as px
import xml.etree.cElementTree as ET

# from streamlit.script_runner import RerunException
# from streamlit_autorefresh import st_autorefresh


st.image("https://assets.crossref.org/logo/crossref-logo-landscape-200.png")
col1,col2 = st.columns([1,1])
scol1,scol2 = st.sidebar.columns([1,1])
st.subheader("**Crossref Submission Diagnostic Tool**")
st.sidebar.subheader("Enter submission and check on the potential errors")
works_url = "works?"
nl = "\n"
timestamp = ('{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now()))
st.markdown(""" <style>div[role="radiogroup"] >  :first-child{display: none !important;}</style>""",unsafe_allow_html=True)
st.write('<style>div.row-widget.stRadio > div{flex-direction:row;}</style>', unsafe_allow_html=True)
custom_container = st.container()
st.markdown('##')
st.markdown('##')


dois = []
not_reg = []
filename_list = ['download']
column_list = []


def init_login():
	with st.container():
		email = col1.text_input("Email Address",key="email")
		password = col2.text_input("Password", type="password", key="password")
	return email,password

def doi_return(r):
	root = ET.fromstring(r.content)
	filename_list.append("dois")
	filename_list.append(timestamp + ".csv")
	for child in root.iter():
		tag = (child.tag).split("}",1)[1]
		if tag == "doi_data":
			doi = child[0].text
			dois.append(doi)
	return dois



def headers():
  return {
      "User-Agent" : "crossref-api-downloader; mailto=support@crossref.org"
  }



def submission_return(email,password):
	username = email + "/root"
	url = 'https://doi.crossref.org/servlet/submissionAdmin?sf=content&submissionID='+ submission + '&usr=' + username + '&pwd=' + password
	# url = 'https://test.crossref.org/servlet/submissionAdmin?sf=content&submissionID='+ submission + '&usr=' + username + '&pwd=' + password
	r = requests.get(url)
	return doi_return(r) 






def return_doi_list(dois):
	if len(dois) > 100:
		st.sidebar.info("List not shown as over 100 DOIs")
		
	else:	
		st.sidebar.subheader(f"Here is the list of {len(dois)} DOIs to check on: ")
		dois = " ".join(dois)
		st.markdown('##')
		st.sidebar.info(f"{dois}")



email,password = init_login()
data_field = st.sidebar.container()
submission = data_field.text_input("Enter your submission log ID")


if submission:
	output = submission_return(email,password)
	st.session_state.dois = output
	return_doi_list(st.session_state.dois)


run_diagnostic = st.button("Run Diagnostic Tool")

# submission = "1528432536"