
import streamlit as st
import requests
import datetime
import pandas as pd
import numpy as np
import urllib.parse
from urllib.parse import unquote
import time
import xml.etree.cElementTree as ET



works_url = "works?"
nl = "\n"



#EMPTY LISTS
filename_list = []





def dict_convert_to_table(results):
	df = pd.DataFrame.from_dict(results,orient='index')
	return df


def headers():
  return {
      "User-Agent" : "crossref-api-downloader; mailto=support@crossref.org"
  }



def split_dois(dois):
	dois = dois.split()
	return dois

	
def convert_from_dict(data):
	for k,v, in data.items():
		if k == "date-parts":
			data = datetime.date(*v[0])
	return data


def return_doi_list(dois):
	if len(dois) > 100:
		st.info(f"List not shown as over 100 DOIs  \n Total DOIs {len(dois)}")
	else:	
		st.sidebar.subheader(f"Here is the list of {len(dois)} DOIs to check on: ")
		dois = " ".join(dois)
		st.markdown('##')
		st.sidebar.info(dois)
		

def data_to_csv(results):
	data = pd.DataFrame(results)
	csv_data = data.to_csv(encoding='utf-8')
	return csv_data


def report_query(dois,report):
	if report == "Registered DOI Check":
		output = doi_check(dois)
	elif report == "DOI owner check":
		output = owner_check(dois)
	elif report == "CY or BY":
		output = cy_by_check(dois)	
	return output


def doi_check(dois):
	for doi in dois:
		output = []
		time.sleep(0.1)
		api_call = "http://api.crossref.org/works/" + doi
		results = requests.get(api_call)
		if results.status_code == 404:
			output.append(doi)
		st.info(f"Out of {len(dois)} DOIs, {len(output)} DOI/DOIs are not registered with us.")
	return output


def owner_check(dois):
	output = []
	for doi in dois:
		try:
			api_call = "http://api.crossref.org/works/" + doi
			results = requests.get(api_call).json()['message']
			owner = results['prefix']
			output.append([doi,owner])
		except ValueError:
			owner = "Not registered"
			output.append([doi,owner])
	return output


def cy_by_check(dois):
	output = []
	cy = []
	by = []
	for doi in dois:	
		try:
			time.sleep(0.1)
			api_call = "http://api.crossref.org/works/" + doi
			results = requests.get(api_call,headers=headers()).json()['message']
			published = results['published']['date-parts'][0][0]
			
			# print(published)
			if published >= 2020:
				reg_fee = "Current Year"
				cy.append([doi,published])
			else:
				reg_fee = "Back Year"
				by.append([doi,published])
			output.append([doi,str(published),reg_fee])
		except ValueError:
			reg_fee = "Not registered"
			published = "Not registered"
			output.append([doi,published,reg_fee])
	results_list("Current Year",cy)
	results_list("Back Year",by)
	return output

def results_list(tag,data):
	with st.expander(f"{tag} DOIs - {len(data)}"):
		df = pd.DataFrame(data)
		st.dataframe(df)
		st.download_button(f"📥  Download {tag} DOIs",data_to_csv(data),mime='text/csv',file_name=f"{tag}_dois_{timestamp}",key=f"{tag}")	


def reports():
	reports_list = "Registered DOI Check","DOI owner check","CY or BY" 
	st.radio("What report should we run",options=reports_list,key="report_select",horizontal=True)
	
def timestamp():
	return ('{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now()))

def main():
	st.subheader(f"Search on DOIs tool")
	dois_input = st.text_area("Enter DOIs here")
	if dois_input:
		st.session_state.dois = split_dois(dois_input)
		return_doi_list(st.session_state.dois)
		reports()
		if len(st.session_state.report_select) > 0:
			run_tool = st.button("Run the report")
			if run_tool:
				results = report_query(st.session_state.dois,st.session_state.report_select)
				with st.spinner("Getting the data..."):
					results_list("Full results table",results)
		else: st.write("No data requested for return, please select elements from list and try again")
			

if __name__ == "__main__":
	main()
		
				
	




			




		
# st.session_state

# https://test.crossref.org/servlet/submissionAdmin?sf=content&submissionID=1433241432