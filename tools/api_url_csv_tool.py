import sys
from ast import parse
import streamlit as st
import requests
import datetime
import pandas as pd
import urllib.parse
from settings import (CR_PRIMARY_COLORS,API_URI)
from urllib.parse import unquote
import plotly.graph_objects as go
import plotly.express as px
from settings import API_URI





def headers():
  return {
      "User-Agent" : "crossref-api-downloader; mailto=support@crossref.org"
  }


def user_url_section():
	st.info("Add your own API link below to retrieve results from the API  \n **NB: Don't include the cursors or rows elements**")
	st.text_input("Add the User URL here",key="user_url")



def data_request(url):
	item_list = []
	cursor = "*"
	rows = "1000"
	while True:    
		if url[-1] == "?":
			full_query= str(f"{url}cursor={cursor}&rows={rows}")
		else:
			full_query= str(f"{url}&cursor={cursor}&rows={rows}")
		res = requests.get(full_query,headers=headers())
		if res.status_code == 200:
			success = "Got JSON...loading results"
		elif str(res.status_code).startswith('5'):
			st.error(f"{res.status_code} Error returned...the API could be down")
			
		else:
			st.warning(res.status_code)
			st.warning("Valid JSON has not been retrieved")
			
		try:
			body = res.json()['message']
			total_results = body['total-results']
			items = body['items']
			if not items:
				break
			cursorRaw = str(body['next-cursor'])
			cursor = urllib.parse.quote(cursorRaw)
			for i in items:
				item_list.append(i)
		except TypeError:
			st.warning(f"The API query is not valid or no results available, please clear form and try again")
			break
	return item_list,total_results,success

def convert_output(output):
	data = pd.DataFrame(output)
	csv_data = data.to_csv(index=False,encoding='utf_8')
	return csv_data

def timestamp():
	return ('{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now()))


def main():
	st.subheader("**Crossref API Query URL Download Tool**")
	# st.markdown("***")
	user_url_section()
	
	if st.session_state.user_url:
		if "url" not in st.session_state:
			st.session_state.url = st.session_state.user_url
	col1,col2,col3 = st.columns([1,1,1])
	run_query = col2.button("Run API Query")
	if run_query:
		api_query = st.session_state.url
		with st.spinner("Retrieving your JSON..."):
			st.write(f'Here is link to the API query you are running:')
			st.info(unquote(api_query))
			st.info(f"**The total results from that query are {data_request(api_query)[1]}**")
			col2.success(data_request(api_query)[3])
			output = data_request(api_query)[0]
		st.subheader(f"Your results are here")
		with st.expander(f"Results"):
			st.json(output)
		filename = f"user_url_query_{timestamp()}.csv"
		data = convert_output(output)
		col2.download_button("Download Ouput",data, mime='text/csv',file_name=filename)

if __name__ == '__main__':
	st.session_state
	main()
	
